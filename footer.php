	<footer>
		<div class="wrapper">

			<div class="hrg">

				<h4><a href="http://www.heavyrestaurantgroup.com/" rel="external">The Heavy Restaurant Group Family</a></h4>

				<div class="hrg-wrapper">

					<div class="row">

						<div class="logo purple">
							<a href="http://purplecafe.com/" rel="external">Purple</a>
						</div>

						<div class="logo barrio">
							<a href="http://barriorestaurant.com/" rel="external">Barrio</a>
						</div>
						
						<div class="logo claret">
							<a href="https://claretseattle.com/" rel="external">Claret</a>
						</div>
					
						<div class="logo fiasco">
							<a href="https://fiascoseattle.com/" rel="external">Fiasco</a>
						</div>

						<div class="logo pablo-y-pablo">
							<a href="http://pabloypablo.com/" rel="external">Pablo y Pablo</a>
						</div>

					</div>

					<div class="row">

						<div class="logo meet-the-moon">
							<a href="http://meetthemooncafe.com/" rel="external">Meet the Moon</a>
						</div>

						<div class="logo the-commons">
							<a href="http://thecommonscafe.com/" rel="external">The Commons</a>
						</div>

						<div class="logo heavy-catering">
							<a href="https://heavycatering.com/" rel="external">Heavy Catering</a>
						</div>

					</div>

				</div>

			</div>

		</div>
	</footer>
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>