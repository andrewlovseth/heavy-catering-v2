<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<?php if(get_field('show_announcement', 'options') == true): ?>

		<section class="announcement">
			<div class="wrapper">
				
				<div class="info">
					<?php the_field('announcement', 'options'); ?>
				</div>

			</div>
		</section>

	<?php endif; ?>

	<section class="fare">
		<div class="wrapper">

			<?php if(have_rows('fare')): while(have_rows('fare')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'option' ): ?>
					
					<article class="option">
						<?php if(get_sub_field('new') == true): ?>
							<div class="new">
								<div class="circle">
									<h4>New!</h4>
								</div>
							</div>

						<?php endif; ?>

						<div class="photo">
							<div class="content">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
						</div>

						<div class="info">
							<div class="info-wrapper">
								<div class="headline">
									<h4><?php the_sub_field('tagline'); ?></h4>
								</div>

								<div class="logos">
									<?php $images = get_sub_field('logos'); if( $images ): ?>
										<?php foreach( $images as $image ): ?>
											<div class="logo">
												<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
											</div>										
										<?php endforeach; ?>
									<?php endif; ?>
								</div>

								<div class="menu">
									<a href="<?php the_sub_field('menu_pdf'); ?>" rel="external">
										<span class="icon"><img src="<?php bloginfo('template_directory') ?>/images/menu.svg" alt="Menu Icon" /></span>
										<span class="label">View Menu</span>
									</a>
								</div>
							</div>

							<div class="cta">
								<a href="<?php the_sub_field('order_link'); ?>" class="btn" rel="external">Order Now</a>
							</div>
						</div>
					</article>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>


		</div>
	</section>

	<section class="about">
		<div class="wrapper">
			
			<div class="headline">
				<h5><?php the_field('about_headline'); ?></h5>

				<div class="logos">
					<?php $images = get_field('about_logos'); if( $images ): ?>
						<?php foreach( $images as $image ): ?>
							<div class="logo">
								<img src="<?php echo $image['url']; ?>" class="<?php echo $image['title']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>										
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>

			<div class="copy">
				<?php the_field('about_copy'); ?>
			</div>

		</div>
	</section>

	<section class="contact">
		<div class="wrapper">

			<div class="headline header">
				<h2><?php the_field('contact_headline'); ?></h2>
			</div>			

			<div class="contact-options">
				<!--
				<div class="option menu">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/images/menu.svg" alt="Menu Icon" />						
					</div>

					<div class="info">
						<div class="headline">
							<h4>All Menus</h4>
						</div>

						<div class="link">
							<a href="<?php the_field('all_menus_pdf'); ?>" rel="exteral">View All Menus (PDF)</a>
						</div>
					</div>
				</div>
				-->

				<div class="option phone">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/images/phone.svg" alt="Phone Icon" />						
					</div>

					<div class="info">
						<div class="headline">
							<h4>Phone</h4>
						</div>

						<div class="link">
							<a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a>
						</div>
					</div>
				</div>


				<div class="option email">
					<div class="icon">
						<img src="<?php bloginfo('template_directory') ?>/images/email.svg" alt="Email Icon" />
					</div>

					<div class="info">
						<div class="headline">
							<h4>Email</h4>
						</div>

						<div class="link">
							<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
						</div>
					</div>
				</div>
			</div>

			<div class="contact-statement">
				<?php the_field('contact_info'); ?>
			</div>

		</div>
	</section>

	<section class="footer-photo">
		<div class="content">
			<img src="<?php $image = get_field('footer_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>		
	</section>

<?php get_footer(); ?>